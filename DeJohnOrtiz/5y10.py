
#..................................................
# Ejercicio 10: Solicitar al usuario un número n y calcular n + nn + nnn
# n = 3 => 3 + 33 + 333 = 369
# John Ortiz Ordoñez https://youtube.com/playlist?list=PL2PZw96yQChwNxk7f8stySXf7Cic8oQk8


n = input('Escriba el valor de n: ')
nn = int('{}{}'.format(n, n))
nnn = int('%s%s%s' % (n, n, n))
n = int(n)
suma = n + nn + nnn
print(suma)
# 2 + 22 + 222 = 246
#............................................


#**************************************************
# Mi aporte, por Rafael Aquino
m = n
mm = str(m) + str(m)
mmm = str(m) + str(m) + str(m)
print(int(m) + int(mm)+ int(mmm))
#FIN Mi aporte, Rafael Aquino
#***************************************************

"""
#...................
# Ejercicio 5: Obtener la representación inversa de una cadena de caracteres.
# Python => nohtyP
# John Ortiz Ordoñez https://youtube.com/playlist?list=PL2PZw96yQChwNxk7f8stySXf7Cic8oQk8

cadena = 'Python'
for i in range(len(cadena) - 1, -1, -1):
    print(cadena[i], end='')

print()
print(cadena[::-1])


#*****************************************************

#Mi aporte: por Rafael Aquino
print("Mi aporte")
#Una forma
print("Una forma")
print("".join(reversed(cadena)))
print("Otra forma")
#Otra forma
for i in reversed(cadena):
    print(i, end= "")

print()

#******************************************************

"""
